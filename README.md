# almalinux-release-synergy RPM

The main branch of this repo is empty, all the fun happens in other branches:

 * [a8](https://gitlab.cern.ch/linuxsupport/rpms/releases/almalinux-release-synergy/-/tree/a8)
 * [a9](https://gitlab.cern.ch/linuxsupport/rpms/releases/almalinux-release-synergy/-/tree/a9)

